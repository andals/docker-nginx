#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

cd $installDstRoot
tar zxvf $pkgRoot/luajit-2.0.3.tar.gz
ln -s luajit-2.0.3 luajit

export LUAJIT_LIB=$installDstRoot/luajit/lib/
export LUAJIT_INC=$installDstRoot/luajit/include/luajit-2.0/

mkdir -p $buildTmpRoot
cd $buildTmpRoot

tar zxvf $srcRoot/openssl-1.0.2h.tar.gz
tar zxvf $srcRoot/nginx-1.8.1.tar.gz
cd nginx-1.8.1
mkdir module_src
cd module_src
tar zxvf $srcRoot/modules/ngx_devel_kit-0.2.19.tgz
tar zxvf $srcRoot/modules/ngx_lua-0.9.7.tgz
tar zxvf $srcRoot/modules/ngx_echo-0.53.tgz
cd ..

./configure --prefix=$installDstRoot/nginx-1.8.1 --with-http_ssl_module --with-openssl=$buildTmpRoot/openssl-1.0.2h --with-http_gzip_static_module --with-ld-opt="-Wl,-rpath,$LUAJIT_LIB" --add-module=$buildTmpRoot/nginx-1.8.1/module_src/ngx_devel_kit-0.2.19/ --add-module=$buildTmpRoot/nginx-1.8.1/module_src/lua-nginx-module-0.9.7/ --add-module=$buildTmpRoot/nginx-1.8.1/module_src/echo-nginx-module-0.53/ --with-debug
make
make install

cd $installDstRoot
tar zcvf nginx-1.8.1.tar.gz nginx-1.8.1
cp nginx-1.8.1.tar.gz $pkgRoot
