#!/bin/bash

if [ ! -d /data/nginx/conf ]
then
    cd /data/nginx
    cp -r /build/pkg/conf ./
    mkdir logs
fi

/usr/local/nginx/sbin/nginx -g "daemon off;"

sleep infinity
