#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

cd $installDstRoot
tar zxvf $pkgRoot/luajit-2.0.3.tar.gz
ln -s luajit-2.0.3 luajit

tar zxvf $pkgRoot/nginx-1.8.1.tar.gz
ln -s nginx-1.8.1 nginx

cd nginx
mkdir pid
mkdir -p /data/nginx
mv conf /data/nginx
mv logs /data/nginx

ln -s /data/nginx/conf conf
ln -s /data/nginx/logs logs

echo -e '\nPATH=$PATH:/usr/local/nginx/sbin' >> $HOME/.bashrc
