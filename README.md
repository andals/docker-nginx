# docker-nginx

[git](https://gitee.com/andals/docker-nginx)

# Init

```
git clone git@gitee.com:andals/docker-nginx.git nginx
cd nginx
prjHome=`pwd`
ngxVer=1.8.1
```

# Build pkg

```
docker run -i -t -v $prjHome/:/build --name=build-nginx-${ngxVer} andals/centos:7 /bin/bash
/build/script/build_pkg.sh
```

# Build image

```
docker build -t andals/nginx:${ngxVer} ./
```

# Run container

```
docker run -d --name=nginx-${ngxVer} --volumes-from=data-home -v /data/nginx:/data/nginx --net=host andals/nginx:${ngxVer}
```
